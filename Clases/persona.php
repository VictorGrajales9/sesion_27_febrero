<?php

class Persona
{
	
	function __construct()
	{
		$this->despida();
	}

	function saludar()
	{
		echo "<pre>";
		echo "Hola <br>";
		echo "</pre>";
	}

	function despida()
	{
		echo "<pre>";
		echo "Adios <br>";
		echo "</pre>";
	}
}