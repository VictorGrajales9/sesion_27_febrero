<?php 

/**
 * 
 */
class Persona_2
{
	
	private $nombre;
	private $apellido;
	private $edad;
	private $genero;
	private $persona;
	function __construct()
	{
		# code...
	}

	function crear_persona(string $nombre, string $apellido, int $edad): string
	{
		$this->nombre = $nombre;
		$this->apellido = $apellido;
		$this->edad = $edad;
		$this->persona = 'Nombre Completo: '.$this->nombre.' '.$this->apellido.', Edad: '.$this->edad;
		return $this->persona;
	}

	function genero(string $genero)
	{
		$this->genero = $genero;
		if ($this->genero == 'M') {
			echo ", Genero: Masculino";	
		}
		elseif ($this->genero == 'F') {
			echo ", Genero: Femenino";
		}
		else {
			echo ", Genero: Otro";				
		}
	}
}