<?php 	
class db
{
	protected $baseDatos,$usuario,$clave,$db;

	function __construct()
	{
		$this->$baseDatos = "la50";
		$this->$usuario = "root";
		$this->$clave = "";

		try {
			$this->$db = new PDO("mysql:host=localhost;dbname=" . $this->baseDatos, $this->usuario, $this->clave, array(PDO::ATRR_PERSITENT=>true, PDO::ATTR_ERRMODE=>true));
		} catch (PDOException $e) {
			echo "Error: " . $e;
		}
	}

	protected function iniciar()
	{
		$this->db->beginTransaction();
	}

	protected function finalizar()
	{
		$this->db->commit();
	}

	protected function preparar($_cadena)
	{
		$_consulta = $this->db->prepare($_cadena);
		$_consulta->execute();
		return $_consulta
	}

	public function consultas($_cadena)
	{
		$this->db->iniciar();
		$_consulta = $this->preparar($_cadena);
		$_arrayConsulta = $_consulta->fetchAll(PDO::FETCH_ASSOC);
		$this->finalizar();
		return $_arrayConsulta;
	}

	public function insertar($_cadena)
	{
		$this->db->iniciar();
		$_insert = $this->preparar($_cadena);
		$_id = $this->db->lastInsertId();
		$this->finalizar();
		return $_id;
	}

	


}

//Modificar al archivo index.php
//--Pt 3, video 1 ---  11;24